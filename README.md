# mw-wf-cli
Command line client for wflash WiFi bootloader

# Installing
Pre-built versions for 64-bit Windows 7 (or later) with Qt support, can be found [in the package registry](https://gitlab.com/doragasu/mw-wf-cli/-/packages).

# Building

To build the tool with Qt support, run:

```bash
$ cd src
$ qmake
$ make
```

Of course you will need to have installed Qt and the development headers for the compilation to succeed.

You can also build the tool only with command-line support by using one of the alternative makefiles, e.g.:

```bash
$ cd src
$ make -f Makefile-noqt
```

# Burning ROMs
`wflash` has built in help. Just launch it and it will tell you the supported options. Of course you will also need a wflash bootloader programmed to a MegaWiFi cartridge, inserted and running on a Genesis/Megadrive consonle. I will detail a bit more this section when I get some more time ¬_¬. If you built the tool with Qt support, you can also launch an easier to use GUI by running `wflash -Q`.

# Author and contributions
This program has been written by doragasu. Contributions are welcome. Please don't hesitate sending a pull request.
